@extends ('layouts.lottery')

@section ('content')
    <section class="u-align-center u-clearfix u-palette-1-base u-section-2" id="sec-a436">
        <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
            <div class="u-clearfix u-expanded-width u-gutter-30 u-layout-wrap u-layout-wrap-1">
                <div class="u-layout">
                    <div class="u-layout-row">
                        <div
                            class="u-align-left u-container-style u-layout-cell u-left-cell u-size-26-md u-size-26-sm u-size-26-xs u-size-28-lg u-size-28-xl u-layout-cell-1">
                            <div class="u-container-layout u-valign-top-sm u-valign-top-xs u-container-layout-1">
                                <h1 class="u-align-left u-custom-font u-font-montserrat u-text u-text-1">Better luck
                                    next time!</h1>
                                <p class="u-align-left u-text u-text-2">Not so lucky today.. bummer..<br>
                                    <br>A wise man once told me: "Failure is the tuition paid for success", so it's
                                    alright! There's always a next time. Want to give it another shot?
                                </p>
                                <a href="https://www.health.com/health/gallery/0,,20566684,00.html#yogurt-recipes-april"
                                   class="u-align-left u-btn u-button-style u-hover-palette-2-base u-white u-btn-1"
                                   target="_blank">re-enter lottery</a>
                            </div>
                        </div>
                        <div
                            class="u-container-style u-layout-cell u-right-cell u-size-32-lg u-size-32-xl u-size-34-md u-size-34-sm u-size-34-xs u-layout-cell-2">
                            <div class="u-container-layout">
                                <div class="u-align-left u-palette-1-light-2 u-shape u-shape-1"></div>
                                <img
                                    src="images/le-pouting-her-lips-while-looking-camera-with-upset-face-dressed-casual-clothes-while-standing-white-background_295783-5329.jpg"
                                    alt="" class="u-border-12 u-border-white u-image u-image-default u-image-1"
                                    data-image-width="626" data-image-height="417">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
