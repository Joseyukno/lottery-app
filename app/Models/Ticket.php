<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class Ticket extends Model
{
    use HasFactory, HasApiTokens;

    protected $fillable = ['name', 'price'];

    /**
     * Get the token associated with the ticket.
     */
    public function token()
    {
        return $this->hasOne(Token::class);
    }


    /**
     * One ticket could win multiple times.
     */
    public function winner()
    {
        return $this->hasMany(Winner::class);
    }
}
