<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    use HasFactory;

    protected $fillable = ['token'];

    /**
     * Get the token associated with the winner.
     */
    public function token()
    {
        return $this->belongsTo(Token::class);
    }
}
