<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class Token extends Model
{
    use HasFactory, HasApiTokens;

    protected $fillable = ['token'];

    /**
     * Get the ticket associated with the token.
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }
}
