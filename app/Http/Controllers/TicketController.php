<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use App\Models\Token;
use App\Models\Winner;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;


class TicketController extends Controller
{
    /**
     * Display a listing of the tickets.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::all();

        //check if ticket exists
        if ($tickets != null) {
            $response = [
                'message' => 'Success: tickets received',
                'tickets' => $tickets
            ];

            return response($response, 200);
        }

        $response = [
            'message' => 'Failed: no tickets found',
        ];

        return response($response, 404);
    }

    /**
     * Store a newly created ticket in storage and generates token for it in another table.
     *
     * @param  Request  $request
     * @return Response
     */
    public function buyTicket(Request $request)
    {
        $data = $request->validate([
            'name' => 'string|required',
            'price' => 'required'
        ]);

        $ticket = Ticket::create([
           'name' => $data['name'],
           'price' => $data['price']
        ]);

        $token = $ticket->createToken('ticketToken')->plainTextToken;

        $ticketToken = Token::create([
           'token' => $token
        ]);

        $response = [
            'message' => "Success: ticket was bought",
            'ticket' => $ticket,
            'token' => $ticketToken->token
        ];

        return response($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Response
     */
    public function show($id)
    {
        $ticket = DB::table('tickets')
            ->join('tokens', 'tickets.id', '=', 'tokens.ticket_id')
            ->select('tickets.*', 'tokens.token')
            ->where('id', $id)
            ->get();

        if ($ticket != null) {
            $response = [
                'message' => "Success: ticket found",
                'ticket' => $ticket
            ];

            return response($response, 200);
        }

        $response = [
            'message' => "Failed: ticket doesn't exist",
        ];

        return response($response, 404);
    }

    /**
     * Display the average price of all the bought tickets.
     *
     * @return Response
     */
    public function average()
    {
        $average = number_format(Ticket::avg('price'), 2);

        if ($average >= 0) {
            $response = [
                'message' => "Success: average buying price calculated",
                'average buying price' => $average
            ];

            return response($response, 200);
        }
        $response = [
            'message' => "failed: no tickets bought",
        ];

        return response($response, 404);
    }

    /**
     * Picks a winner from all the entries, and save it in the database
     *
     * @return Response
     */
    public function pick()
    {
        //return all the tickets
        $tickets = DB::table('tickets')
            ->join('tokens', 'tickets.id', '=', 'tokens.ticket_id')
            ->get();

        //sum all the prices
        $total = Ticket::sum('price') * 100;

        //pick a random number
        $randomNumber = mt_rand(1, $total);

        //loop through the numbers and return winning ticket
        foreach ($tickets as $ticket) {
            $randomNumber -= (int)($ticket->price * 100);
            if ($randomNumber <= 0) {

                //add winner to the database
                Winner::create([
                   'token' => $ticket->token
                ]);

                $response = [
                    'message' => 'Success: winner has been selected',
                    'winning ticket' => $ticket
                ];
                return response($response, 200);
            }
        }

        //if there's no winner (very unlikely), return no winner
        $response = [
            'winning ticket' => "no winner selected!"
        ];
        return response($response, 200);
    }

    /**
     * gives user the result of the lottery
     *
     * @param $token
     * @return Response
     */
    public function resultJSON($token)
    {
        $winner = Winner::orderBy('id', 'desc')->first();

        //check if entered ticket exists
        $ticket = Token::where('token', $token)->first();
        if ($ticket != null) {
            if ($token == $winner->token) {
                $response = [
                    'message' => 'Success: entry validated',
                    'result' => 'Congratulations! You have won the jackpot!'
                ];
            } else {
                $response = [
                    'result' => 'Too bad! Better luck next time!'
                ];
            }
            return response($response, 200);
        } else {
            $response = [
                'message' => 'Failed: not found',
                'result' => 'The entered token does not exist'
            ];

            return response($response, 404);
        }
    }

    /**
     * gives user the result of the lottery as a HTML page
     *
     * @param $token
     * @return Response
     */
    public function resultHTML($token)
    {
        $winner = Winner::orderBy('id', 'desc')->first();

        //check if entered ticket exists
        $ticket = Token::where('token', $token)->first();
        if ($ticket != null) {
            if ($token == $winner->token) {
                return view('winner');
            } else {
                return view('loser');
            }
        } else {
            $response = [
                'message' => 'Failed: not found',
                'result' => 'The entered token does not exist'
            ];

            return response($response, 404);
        }
    }
}
