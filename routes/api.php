<?php

use App\Http\Controllers\TicketController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:sanctum']], function () {

});

//endpoint for buying tickets
Route::post('/buy', [TicketController::class, 'buyTicket']);

//endpoints for seeing tickets
Route::get('/tickets', [TicketController::class, 'index']);
Route::get('/tickets/{id}', [TicketController::class, 'show']);

//endpoint for checking the average buying price
Route::get('/average', [TicketController::class, 'average']);

//endpoint for selecting winner
Route::get('/pick', [TicketController::class, 'pick']);

//endpoints for seeing the result of your ticket
Route::get('/result/{token}/json', [TicketController::class, 'resultJSON']);
Route::get('/result/{token}', [TicketController::class, 'resultHTML']);
